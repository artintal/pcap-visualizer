class IPRep
  attr_accessor :ip_addr, :x, :y, :width, :window
  TOP_COLOR = Gosu::Color.new(0xFF1EB1FA)
  BOTTOM_COLOR = Gosu::Color.new(0xFF1D4DB5)

  def initialize(window, x, y, width, ip_addr)
    self.x = x
    self.y = y
    self.width = width
    self.window = window
    self.ip_addr = ip_addr
    @font = Gosu::Font.new(window, "Arial", 22)
  end

  def draw
    window.draw_quad(
    x, y, TOP_COLOR,
    x+width, y, TOP_COLOR,
    x, y+width, BOTTOM_COLOR,
    x+width, y+width, BOTTOM_COLOR,
    0)
    @font.draw(ip_addr, x-15, y-15, 1.0, 1.0, 1.0, Gosu::Color::BLACK)
  end    
end

