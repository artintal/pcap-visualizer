require 'gosu'
require 'rubygems'
require 'packetfu'
require './ip_rep'
require './ip_connection'

class GameWindow < Gosu::Window
  WIDTH = 1280
  HEIGHT = 880
  TITLE = "Networking Visualization"
  WHITE_COLOR = Gosu::Color.new(0xFFFFFFFF)
  attr_accessor :packet_reps, :connections, :finished

  def initialize
    super WIDTH, HEIGHT, false
    self.caption = TITLE
    @num_packets = 0
    self.packet_reps = []
    self.connections = []
    self.finished = false
    @font = Gosu::Font.new(self, "Arial", 20)
    @file_name = ARGV.first
    @packet_command = ARGV[1]
    @packet_num = ARGV[2]
    if @file_name and File.readable?(@file_name)
      parse_packet_ip_and_connections(get_packet_array(@file_name))
    end
  end
  
  def draw
    draw_background
    if @file_name
      @font.draw("Each blue box is a unique ip from the tcp and udp packets in the pcap file. Connections are drawn with lines. Blue is source IP and red is destination IP.", 10, 10, 1, 1, 1, Gosu::Color::BLACK)
      @font.draw("Pcap opened: " + @file_name, 10, 30, 1, 1, 1, Gosu::Color::BLACK)
    else
      @font.draw("Please provide a pcap file as an argument", 10, 30, 1, 1, 1, Gosu::Color::BLACK)
    end
    if finished
      if packet_reps.any?
        packet_reps.each do |pr|
          pr.draw
        end
      end
      if connections.any?
        connections.each do |c|
          c.draw
        end
      end
    end
  end

  def draw_background
    draw_quad(
    0, 0, WHITE_COLOR,
    WIDTH, 0, WHITE_COLOR,
    0, HEIGHT, WHITE_COLOR,
    WIDTH, HEIGHT, WHITE_COLOR,
    0)
  end
  
  def get_packet_array(file)
    puts "Opening the packet file"
    file = File.open(file) {|f| f.read}
    pcapfile = PacketFu::PcapPackets.new
    pcapfile.read(file)
    "Done reading the file."
    return pcapfile
  end

  def parse_packet_ip_and_connections(pcapfile)
    ip_map = {}

    puts "Starting to parse the packets"
    pcapfile.each_with_index do |p, i|
      pkt = PacketFu::Packet.parse(p.data) 
      if @packet_command != nil and @packet_command == "start"
        if @packet_num != nil and i < @packet_num.to_i
          next
        end
      elsif @packet_command != nil and @packet_command == "stop"
        if @packet_num != nil and i >= @packet_num.to_i
          break
        end
      end
      puts i
      if pkt.is_tcp? or pkt.is_udp?# or pkt.is_arp?
        source_method = "ip_saddr"
        dest_method = "ip_daddr"
        if pkt.is_arp?
          source_method = "arp_saddr_ip"
          dest_method = "arp_dest_ip"
        end
        #Check if any of these packets ip addresses are unique
        if not ip_map.key?(pkt.send(source_method))
          #pack_rep = IPRep.new(self, 10+ip_map.keys.count*30, 10+ip_map.keys.count*30, 25, pkt.ip_saddr)
          pack_rep = IPRep.new(self, rand(0..1230), rand(50..830), 50, pkt.send(source_method))
          ip_map["#{pkt.send(source_method)}"] = pack_rep
          @packet_reps << pack_rep
        end
        if not ip_map.key?(pkt.send(dest_method))
          #pack_rep = IPRep.new(self, 10, 10+ip_map.keys.count*30, 25, pkt.ip_daddr)
          pack_rep = IPRep.new(self, rand(0..1230), rand(50..830), 50, pkt.send(dest_method))
          ip_map["#{pkt.send(dest_method)}"] = pack_rep
          @packet_reps << pack_rep
        end
        #Create a connection between
        connection = IPConnection.new(self, pkt.send(source_method), pkt.send(dest_method), ip_map)
        @connections << connection
      end
    end
    self.finished = true
    return
  end
end

window = GameWindow.new
window.show
