class IPConnection
  attr_accessor :ip_source, :ip_dest, :ip_map, :window
  FROM_COLOR = Gosu::Color.new(0xFF1EB1FA)
  TO_COLOR = Gosu::Color::RED

  def initialize(window, ip_source, ip_dest, ip_map)
    self.window = window
    self.ip_source = ip_source
    self.ip_dest = ip_dest
    self.ip_map = ip_map
    self.window = window
  end
   
  def draw
    window.draw_line(
    ip_map[ip_source].x+ip_map[ip_source].width/2,
    ip_map[ip_source].y+ip_map[ip_source].width/2,
    FROM_COLOR,
    ip_map[ip_dest].x+ip_map[ip_dest].width/2,
    ip_map[ip_dest].y+ip_map[ip_dest].width/2,
    TO_COLOR,
    0)
  end
end
